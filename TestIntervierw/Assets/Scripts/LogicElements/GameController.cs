﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

//classe responsavel por controlar inicio e fim de jogo e armazenar informaçoes sobre o level que esta sendo jogado
public class GameController : MonoBehaviour
{
    public static GameController instance {private set; get;}
    public GameObject nodeObj, nextLvlPopUp;
    public List<Node> nodes;
    private Transform canvas;

    private Level levelData;

    void Start()
    {
        if(instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(instance);
            instance = this;
        }

        canvas = GameObject.FindWithTag("Canvas").transform;
        levelData = Resources.Load<Level>("Levels/Level " + PlayerPrefs.GetInt("CurrentLevel", 1));
        nodes = new List<Node>();
        StartGame();
    }

    public void StartGame()
    {
        GameObject obj = Resources.Load<GameObject>("Prefabs/Node");

        var i = 0;

        foreach (Vector3 a in levelData.nodes)
        {
            var objRef = Instantiate(obj, canvas);
            Node node = objRef.GetComponent<Node>();
            nodes.Add(node);

            node.isEnergyNode = levelData.isEnergyNode[i];
            objRef.GetComponent<RectTransform>().anchoredPosition = a;
            node.amountOfEnergy = levelData.amountOfEnergy[i];
            node.isEndNode = levelData.isEndNode[i];
            node.index = levelData.index[i];
            objRef.transform.Find("Text").GetComponent<TextMeshProUGUI>().text = levelData.amountOfEnergy[i].ToString();
            i++;
        }
    }

    public void EndGame()
    {
        Debug.Log("endver");
        var count = 0;
        foreach (Node a in nodes)
        {
            if (a.isEnergyNode)
            {
                count++;
                if (count == nodes.Count)
                {
                    Debug.Log("gameCompleted");

                    if (PlayerPrefs.GetInt("CompletedLevel", 1) < PlayerPrefs.GetInt("CurrentLevel", 1)) 
                    {
                        PlayerPrefs.SetInt("CompletedLevel", PlayerPrefs.GetInt("CurrentLevel", 1));
                    }

                    StartCoroutine(NextLevelPopUp());
                }
            }
        }
    }

    IEnumerator NextLevelPopUp()
    {
        var lineObjs = GameObject.FindGameObjectsWithTag("Line");

        foreach (GameObject lineRender in lineObjs)
        {
            lineRender.GetComponent<LineRenderer>().startColor = Color.green;
            lineRender.GetComponent<LineRenderer>().endColor = Color.green;
        }

        yield return new WaitForSeconds(1f);
       
        foreach (GameObject line in lineObjs)
        {
            Destroy(line);
        }

        Instantiate(nextLvlPopUp, canvas);
    }

    //acesso as informaçoes do scritable object
    public Level GetLevel()
    {
        return levelData;
    }
}
