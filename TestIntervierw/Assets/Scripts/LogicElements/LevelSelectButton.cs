﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

//Classe responsavel pela logica dos botoes de seleçao de level

public class LevelSelectButton : MonoBehaviour
{
    public int level;
    public Color defaultColor, blocked;
    void Start()
    {
        //verifica o level que o jogador completou e bloqueia os botoes caso o level nao esteja disponivel
        if(PlayerPrefs.GetInt("CompletedLevel", 1) < level)
        {
            GetComponent<Image>().color = blocked;
            GetComponent<Button>().enabled = false;
        }
        else
        {
            GetComponent<Image>().color = defaultColor;
            GetComponent<Button>().enabled = true;
        }
    }

    public void StartLevel()
    {
        PlayerPrefs.SetInt("CurrentLevel", level);

        SceneManager.LoadScene("GameScene");
    }
}
