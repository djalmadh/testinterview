﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;

//Classe responsavel por controlar os nodes e suas conexoes
public class Node : MonoBehaviour
{
    public bool isEnergyNode,isEndNode;
    public int amountOfEnergy,index;

    public GameObject LineObj;
    public LineRenderer line;
    public Vector2 startPos, EndPos;
    private Transform canvasTransform;
    private Canvas canvas;
    public Node connectedNode;

    private void Start()
    {
        canvasTransform = GameObject.FindWithTag("Canvas").transform;
        canvas = GameObject.FindWithTag("Canvas").GetComponent<Canvas>();
    }

    public void OnBeginDrag(BaseEventData data)
    {
        Node Node = transform.GetComponent<Node>();
        startPos = Vector2.zero;
        EndPos = Vector2.zero;

        //verifica se existe uma node conectada e desfaz a conexao
        if (connectedNode != null)
        {
            connectedNode.amountOfEnergy -= 1;
            amountOfEnergy += 1;
            if (connectedNode.amountOfEnergy == 0)
            {
                connectedNode.isEnergyNode = false;
                connectedNode.transform.Find("Text").GetComponent<TextMeshProUGUI>().text = connectedNode.amountOfEnergy.ToString();
            }

            transform.Find("Text").GetComponent<TextMeshProUGUI>().text = amountOfEnergy.ToString();
            connectedNode = null;
        }

        //setup inicial da linha
        if (line == null && Node.amountOfEnergy > 0)
        {
            var lineRef = Instantiate(LineObj, canvasTransform);
            lineRef.transform.localPosition = transform.localPosition;
            line = lineRef.GetComponent<LineRenderer>();

            line.positionCount = 2;
            startPos = lineRef.transform.localPosition;
            line.SetPosition(0, startPos);
        }
    }
    
    public void OnDrag(BaseEventData data)
    {
        //Ajuste da linha atraves da posiçao do mouse
        if (line != null)
        {
            PointerEventData eventData = (PointerEventData)data;

            EndPos += eventData.delta / canvas.scaleFactor;
            line.SetPosition(0, EndPos);
        }
    }

    
    public void OnEndDrag(BaseEventData data)
    {
        //Resetar Linha se nenhuma node foi conectada
        if (connectedNode == null)
        {
            Destroy(line);
            line = null;
            connectedNode = null;
        }
    }

    public void OnDrop(BaseEventData data)
    {
        //Verifica se existe uma node disponivel para a conexao;
        //Se existe faz o ajuste dos dados caso contrario reseta a linha;
        PointerEventData eventData = (PointerEventData)data;
        Node node = eventData.pointerDrag.GetComponent<Node>();
        Node dropNode = transform.GetComponent<Node>();

        if (node.isEnergyNode && node.amountOfEnergy > 0 && dropNode.amountOfEnergy < 1 && !node.isEndNode && node.index + 1 == dropNode.index)
        {
            dropNode.isEnergyNode = true;
            dropNode.amountOfEnergy += 1;
            node.connectedNode = dropNode;
            transform.Find("Text").GetComponent<TextMeshProUGUI>().text = dropNode.amountOfEnergy.ToString();
            node.amountOfEnergy -= 1;
            node.transform.Find("Text").GetComponent<TextMeshProUGUI>().text = node.amountOfEnergy.ToString();            
        }
        else if(node.line !=null)
        {
            Destroy(node.line.gameObject);
            node.line = null;
        }      
        GameController.instance.EndGame();
    }
}
