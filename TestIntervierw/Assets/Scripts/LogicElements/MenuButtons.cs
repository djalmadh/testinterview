﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

//Classe responsavel pela logica dos botoes dos menus

public class MenuButtons : MonoBehaviour
{
    public GameObject lvlMenu;

    public void PlayGame()
    {
        lvlMenu.SetActive(true);
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    public void NextLevel()
    {
        var lvlAmount = Resources.LoadAll<Level>("Levels").Length;
        if (PlayerPrefs.GetInt("CurrentLevel", 1) + 1 <= lvlAmount)
        {
            PlayerPrefs.SetInt("CurrentLevel", PlayerPrefs.GetInt("CurrentLevel", 1) + 1);
        }
        SceneManager.LoadScene("GameScene");
    }

    public void ReturnToMain()
    {
        SceneManager.LoadScene("MenuScene");
    }

}
