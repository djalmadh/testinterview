﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Classe com Informaçoes dos levels

[CreateAssetMenu(fileName = "Level", menuName = "Level")]
public class Level : ScriptableObject
{
    public List<Vector3> nodes;
    public List<bool> isEnergyNode, isEndNode;
    public List<int> amountOfEnergy, index;
}
