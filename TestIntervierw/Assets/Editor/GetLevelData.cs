﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

//editor tool para facilitar a criaçao de levels
public class GetLevelData : EditorWindow
{
    Level levelData;
    int levelNumber = 0;

    [MenuItem("Window/LevelData")]
    public static void ShowWindow()
    {
        GetWindow<GetLevelData>("LevelData");
    }

    private void OnGUI()
    {
        levelNumber = EditorGUILayout.IntField("LevelNumber", levelNumber);
       

        //Botao responsavel por criar o .asset file para guardar as informaçoes do level
        if (GUILayout.Button("CreateLevelAssetFile"))
        {
            if (levelData == null)
            {
                levelData = ScriptableObject.CreateInstance<Level>();
                AssetDatabase.CreateAsset(levelData, $"Assets/Resources/Levels/Level "+ levelNumber +".asset");
                levelData.isEnergyNode = new List<bool>();
                levelData.nodes = new List<Vector3>();
                levelData.amountOfEnergy = new List<int>();
            }
            levelData = null;
        }
        //botao responsavel por salvar as informaçoes dos nodes em cena
        if (GUILayout.Button("SaveData"))
        {
            levelData = Resources.Load<Level>("Levels/Level "+ levelNumber);

            levelData.isEnergyNode.Clear();
            levelData.nodes.Clear();
            levelData.amountOfEnergy.Clear();
            levelData.isEndNode.Clear();
            levelData.index.Clear();

            if (levelData != null)
            {
                var nodes = GameObject.FindGameObjectsWithTag("Node");
                foreach (GameObject a in nodes)
                {
                    levelData.isEnergyNode.Add(a.GetComponent<Node>().isEnergyNode);
                    levelData.nodes.Add(a.GetComponent<RectTransform>().anchoredPosition);
                    levelData.amountOfEnergy.Add(a.GetComponent<Node>().amountOfEnergy);
                    levelData.isEndNode.Add(a.GetComponent<Node>().isEndNode);
                    levelData.index.Add(a.GetComponent<Node>().index);
                }
            }

            EditorUtility.SetDirty(levelData);
            levelData = null;
        }
        //botao responsavel por carregar o level
        if (GUILayout.Button("LoadObjects"))
        {
            levelData = Resources.Load<Level>("Levels/Level " + levelNumber);
            GameObject obj = Resources.Load<GameObject>("Prefabs/Node");
            Transform canvas = GameObject.FindWithTag("Canvas").transform;

            var i = 0;
           
            foreach(Vector3 a in levelData.nodes)
            {
                var objRef = PrefabUtility.InstantiatePrefab(obj,canvas) as GameObject;
                objRef.GetComponent<Node>().isEnergyNode = levelData.isEnergyNode[i];
                objRef.GetComponent<Node>().amountOfEnergy = levelData.amountOfEnergy[i];
                objRef.GetComponent<Node>().isEndNode = levelData.isEndNode[i];
                objRef.GetComponent<Node>().index = levelData.index[i];
                objRef.GetComponent<RectTransform>().anchoredPosition = a;
                i++;
            }
            
        }
        //botao responsavel por apagar os nodes em cena
        if (GUILayout.Button("DeleteObjects"))
        {
            var nodesInScene = GameObject.FindGameObjectsWithTag("Node");

            foreach(GameObject a in nodesInScene)
            {
                DestroyImmediate(a);
            }
        }
    }
}
